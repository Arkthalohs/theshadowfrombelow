﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class FoodBehaviour : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    public bool FollowMouse;
    private Vector2 _offset;
    private Vector3 _startPosition;
    public Action OnEat;

    public void OnPointerDown(PointerEventData eventData)
    {
        FollowMouse = true;
        _offset = transform.position - Input.mousePosition;
        _startPosition = transform.position;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        // This is terrible.
        var character = transform.parent.parent.GetComponentsInChildren<CharacterBehaviour>().
            Where(x => x.isActiveAndEnabled && Intersect(x.GetComponent<RectTransform>())).FirstOrDefault();
        if (character)
        {
            Destroy(gameObject);
            var c = character.GetComponent<CharacterBehaviour>();
            c.Character.Dice = Mathf.Min(Character.MaxDice, c.Character.Dice + Mathf.Max(0, 3 - character.Character.Relics.Sum(r => r.FoodReduction)));
            OnEat();
        }

        transform.position = _startPosition;
        FollowMouse = false;
    }

    private bool Intersect(RectTransform rect)
    {
        var rt = GetComponent<RectTransform>();
        var corners = new Vector3[4];
        rt.GetWorldCorners(corners);
        var rect1 = new Rect(corners[0].x, corners[0].y, corners[2].x - corners[0].x, corners[2].y - corners[0].y);
        rect.GetWorldCorners(corners);
        var rect2 = new Rect(corners[0].x, corners[0].y, corners[2].x - corners[0].x, corners[2].y - corners[0].y);
        return rect1.Overlaps(rect2);
    }

    private void Update()
    {
        if (FollowMouse)
        {
            transform.position = Input.mousePosition + (Vector3)_offset;
        }
    }
}
