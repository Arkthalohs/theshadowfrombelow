﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class GameManagerBehaviour : MonoBehaviour
{
    public CharacterBehaviour CharacterPrefab;
    public CharacterBehaviour ActiveCharacter;
    public List<InteractableRoomBehaviour> Rooms;

    public Text DaysRemainingText;
    public Text FuelGatheredText;
    public Text SurvivorsRemainingText;
    public Text TemperatureText;
    public Text TemperaturePenaltyText;
    public Text FoodText;
    public Text RelicsText;
    public Text LossesText;

    public int DaysRemaining = 7;
    public int FuelGathered = 0;
    public int Temperature = 25;
    public int Food = 3;
    public int ExpeditionCost = 500;
    public List<CharacterBehaviour> Survivors;
    public MouseManagerBehaviour MouseManager;
    public DiceRoller DiceRoller;
    public RectTransform RelicScreen;
    public RectTransform FoodScreen;
    public RectTransform FoodHolder;
    public FoodBehaviour FoodPrefab;
    public RelicSelector RelicSelector;
    public EndScreenBehaviour EndScreen;
    public RescueScreen RescueScreen;

    private float? _endTurnTime;

    public int Relics
    {
        get
        {
            return Survivors.Sum(x => x.Character.Relics.Count);
        }
    }

    public void SelectCharacter(CharacterBehaviour c)
    {
        if (c != null && c.Character.TurnTaken)
        {
            return;
        }

        if (ActiveCharacter != null)
        {
            ActiveCharacter.Glow.enabled = false;
        }

        ActiveCharacter = c;
        if (ActiveCharacter != null)
        {
            ActiveCharacter.Glow.enabled = true;
            foreach (var room in Rooms)
            {
                if (ActiveCharacter.Character.CurrentRoom.GetPathToTargetRoom(room).Any(x => x.HasCrisis))
                {
                    room.SetColors(MouseManager.DangerColor);
                }
                else
                {
                    room.SetColors(MouseManager.DefaultColor);
                }
            }
        }
    }

    public void MoveActiveCharacterToRoom(InteractableRoomBehaviour room)
    {
        if (ActiveCharacter == null)
        {
            return;
        }

        var path = ActiveCharacter.Character.CurrentRoom.GetPathToTargetRoom(room);
        ////var str = "";
        ////foreach (var p in path)
        ////{
        ////    str += p.CurrentRoom.Name + ", ";
        ////}

        ////Debug.Log(str);
        if (path.Any(x => x.HasCrisis))
        {
            ActiveCharacter.Character.Health--;
            if (ActiveCharacter.Character.Health <= 0)
            {
                SelectCharacter(null);
                return;
            }
        }

        ActiveCharacter.Character.CurrentRoom = room;
        ActiveCharacter.Character.TurnTaken = true;
        ActiveCharacter.transform.SetParent(room.CharacterContainer);

        DiceRoller.Show(this, ActiveCharacter, room);
        ////StartCoroutine(InteractInRoomCoroutine(ActiveCharacter, room));
        SelectCharacter(null);
    }

    private void Start()
    {
        foreach (var c in GameObject.FindObjectsOfType<CharacterBehaviour>())
        {
            Destroy(c.gameObject);
        }

        Survivors = new List<CharacterBehaviour>();
        var characters = new Character[]
        {
            new Character("Henry", "Biologist", "+1 Food", "5↑", x => x >= 5, (x, y) => 
            {
                x.Food++;
            }),
            new Character("Agatha", "Engineer", "Fix room for 5", "1↑", x => true, (x, y) => 
            {
                if (y.Character.CurrentRoom.HasCrisis)
                {
                    y.Character.CurrentRoom.CurrentCrisis.Difficulty -= 5;
                }
            }),
            new Character("Wilbur", "Climatologist", "Ignore cold effects", "", x => false, null, coldResist: true),
            new Character("Lavinia", "Scholar", "+2 rerolls", "", null, null, 4)
        };

        characters = characters.OrderBy(x => Random.Range(0, 100)).ToArray();

        Survivors.Add(AddCharacter(characters[0], 5, 4, RandomRoom()));
        Survivors.Add(AddCharacter(characters[1], 5, 4, RandomRoom()));
        Survivors.Add(AddCharacter(characters[2], 5, 4, RandomRoom()));
        Survivors.Add(AddCharacter(characters[3], 5, 4, RandomRoom()));
        DealPain(Survivors[0], 1);
        DealPain(Survivors[1], 2);
        DealPain(Survivors[2], 4);
        DealPain(Survivors[3], 4);

        RandomRoom(true).CurrentCrisis = Crisis.Random(Random.Range(5, 11), 1);
        RandomRoom(true).CurrentCrisis = Crisis.Random(Random.Range(10, 16), 1);
    }

    private void DealPain(CharacterBehaviour character, int amountOfPain)
    {
        for (var i = 0; i < amountOfPain; i++)
        {
            if (Random.Range(0, 2) == 0 && character.Character.Dice > 1)
            {
                character.Character.Dice--;
            }
            else
            {
                character.Character.Health--;
            }
        }
    }

    private InteractableRoomBehaviour RandomRoom(bool onlyNonCrisis = false)
    {
        var roomList = Rooms.Where(x => !onlyNonCrisis || !x.HasCrisis).ToList();
        if (roomList.Count == 0)
        {
            Debug.LogError("There are no rooms without crises!");
            return null;
        }

        return roomList[Random.Range(0, roomList.Count)];
    }

    private void Update()
    {
        // Clear out the dead.
        Survivors = Survivors.Where(x => x).ToList();
        DaysRemainingText.text = DaysRemaining.ToString();
        FuelGatheredText.text = FuelGathered.ToString();
        SurvivorsRemainingText.text = Survivors.Count.ToString();
        TemperatureText.text = Temperature + "°C";
        FoodText.text = Food.ToString();
        RelicsText.text = "$" + (Relics * 100 + Survivors.Sum(x => x.Character.Relics.Count(r => r.Researched) * 200));
        LossesText.text = "-$" + ExpeditionCost.ToString();

        if (_endTurnTime.HasValue && _endTurnTime.Value < Time.time)
        {
            foreach (var s in Survivors)
            {
                s.Character.TurnTaken = false;
                foreach (var r in s.Character.Relics)
                {
                    r.TimesResolved = 0;
                }
            }

            FoodScreen.gameObject.SetActive(true);
            if (DaysRemaining < 2 || Relics > 4)
            {
                FoodScreen.GetComponentInChildren<Text>().text = "Time To Die";
            }

            for (var i = 0; i < Food;i++)
            {
                var food = Instantiate(FoodPrefab);
                food.transform.SetParent(FoodHolder);
                food.transform.localScale = Vector3.one;
                food.OnEat = () => { Food--; };
            }

            var characters = FoodScreen.GetComponentsInChildren<CharacterBehaviour>();

            for  (var i = 0; i < characters.Length; i++)
            {
                characters[i].gameObject.SetActive(i < Survivors.Count);
                if (i < Survivors.Count)
                {
                    characters[i].Character = Survivors[i].Character;
                }
            }

            _endTurnTime = null;
            if (DaysRemaining == 0)
            {
                RescueScreen.Show(this);
                gameObject.SetActive(false);
            }
        }

        if (Survivors.Count == 0)
        {
            EndScreen.EndGame(this, new List<CharacterBehaviour>());
            gameObject.SetActive(false);
        }

        if (Survivors.All(x => x.Character.TurnTaken) && !DiceRoller.isActiveAndEnabled && !_endTurnTime.HasValue)
        {
            Debug.Log("End turn!");
            _endTurnTime = Time.time + 1;
            foreach (var room in Rooms)
            {
                if (room.HasCrisis)
                {
                    room.CurrentCrisis.TurnComplete(this);
                    ExpeditionCost += 100;
                }

                room.CurrentRoom.CurrentDiceValues.Clear();
            }

            DaysRemaining--;

            var coldSurvivors = Survivors.Where(x => !x.Character.ColdResistant);
            if (Temperature > 20) // 68F
            {
                // Cold, but not terrible.
            }
            else if (Temperature >= 0) // 32F
            {
                foreach (var s in coldSurvivors)
                {
                    s.Character.Dice = Mathf.Max(s.Character.Dice - 1, 1);
                }
            }
            else if (Temperature >= -20) // 14F
            {
                foreach (var s in coldSurvivors)
                {
                    s.Character.Dice = Mathf.Max(s.Character.Dice - 1, 1);
                    s.Character.Health -= 1;
                }
            }
            else if (Temperature >= -40) // -40F
            {
                foreach (var s in coldSurvivors)
                {
                    s.Character.Dice = Mathf.Max(s.Character.Dice - 2, 1);
                    s.Character.Health -= 1;
                }
            }
            else if (Temperature >= -60) // -76F
            {
                foreach (var s in coldSurvivors)
                {
                    s.Character.Dice = Mathf.Max(s.Character.Dice - 3, 1);
                    s.Character.Health -= 1;
                }
            }
            else
            {
                foreach (var s in coldSurvivors)
                {
                    s.Character.Dice = Mathf.Max(s.Character.Dice - 3, 1);
                    s.Character.Health -= 2;
                }
            }

            if (DaysRemaining >= 7)
            {
                RandomRoom(true).CurrentCrisis = Crisis.Random(Random.Range(5, 11), 1);
                RandomRoom(true).CurrentCrisis = Crisis.Random(Random.Range(3, 11), 1);
            }
            else if (DaysRemaining >= 5)
            {
                RandomRoom(true).CurrentCrisis = Crisis.Random(Random.Range(9, 20), 2);
                RandomRoom(true).CurrentCrisis = Crisis.Random(Random.Range(5, 11), 1);
            }
            else if (DaysRemaining >= 3)
            {
                RandomRoom(true).CurrentCrisis = Crisis.Random(Random.Range(9, 20), 2);
                RandomRoom(true).CurrentCrisis = Crisis.Random(Random.Range(8, 20), 1);
                RandomRoom(true).CurrentCrisis = Crisis.Random(Random.Range(2, 8), 1);
            }
            else
            {
                RandomRoom(true).CurrentCrisis = Crisis.Random(Random.Range(5, 11), 1);
                RandomRoom(true).CurrentCrisis = Crisis.Random(Random.Range(10, 16), 2);
                RandomRoom(true).CurrentCrisis = Crisis.Random(Random.Range(14, 22), 2);
            }
        }



        if (Temperature > 20) // 68F
        {
            TemperaturePenaltyText.text = "Healthy temperature";
        }
        else if (Temperature >= 0) // 32F
        {
            TemperaturePenaltyText.text = "Starting to get cold";
        }
        else if (Temperature >= -20) // 14F
        {
            TemperaturePenaltyText.text = "-1 Die ALL";
        }
        else if (Temperature >= -40) // -40F
        {
            TemperaturePenaltyText.text = "-1 Die ALL\n-1 Health ALL";
        }
        else if (Temperature >= -60) // -76F
        {
            TemperaturePenaltyText.text = "-2 Die ALL\n-1 Health ALL";
        }
        else
        {
            TemperaturePenaltyText.text = "-2 Die ALL\n-2 Health ALL";
        }
    }

    private CharacterBehaviour AddCharacter(Character character, int startingHealth, int startingDice, InteractableRoomBehaviour room)
    {
        var characterGO = Instantiate<CharacterBehaviour>(CharacterPrefab);
        characterGO.Character = character;
        characterGO.RelicScreen = RelicScreen;
        character.Health = startingHealth;
        character.Dice = startingDice;

        characterGO.GetComponent<Button>().onClick.AddListener(new UnityEngine.Events.UnityAction(() =>
        {
            SelectCharacter(characterGO);
        }));
        characterGO.Character.CurrentRoom = room;
        characterGO.transform.SetParent(room.CharacterContainer);
        characterGO.transform.localScale = Vector3.one;
        return characterGO;
    }
}
