﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ClickAndDrag : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    public bool FollowMouse;
    public List<RectTransform> Parents;
    private Vector2 _offset;
    private Vector3 _startPosition;

    public void OnPointerDown(PointerEventData eventData)
    {
        FollowMouse = true;
        _offset = transform.position - Input.mousePosition;
        _startPosition = transform.position;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        // This is terrible.
        var group = Parents.Where(x => Intersect(x.GetComponent<RectTransform>())).ToList();
        if (group.Count == 1 && transform.parent != group[0].transform)
        {
            transform.SetParent(group[0].transform);
            FollowMouse = false;
            return;
        }
        else
        {
            var result = group.Where(x =>
            {
                var corners = new Vector3[4];
                x.GetWorldCorners(corners);
                var rect2 = new Rect(corners[0].x, corners[0].y, corners[2].x - corners[0].x, corners[2].y - corners[0].y);
                if (rect2.Contains(Input.mousePosition))
                {
                    return true;
                }

                return false;
            }).ToList();

            if (result.Count == 1 && transform.parent != result[0].transform)
            {
                transform.SetParent(result[0].transform);
                FollowMouse = false;
                return;
            }
        }

        transform.position = _startPosition;
        FollowMouse = false;
    }

    private bool Intersect(RectTransform rect)
    {
        var rt = GetComponent<RectTransform>();
        var corners = new Vector3[4];
        rt.GetWorldCorners(corners);
        var rect1 = new Rect(corners[0].x, corners[0].y, corners[2].x - corners[0].x, corners[2].y - corners[0].y);
        rect.GetWorldCorners(corners);
        var rect2 = new Rect(corners[0].x, corners[0].y, corners[2].x - corners[0].x, corners[2].y - corners[0].y);
        return rect1.Overlaps(rect2);
    }

    private void Update()
    {
        if (FollowMouse)
        {
            transform.position = Input.mousePosition + (Vector3)_offset;
        }
    }
}
