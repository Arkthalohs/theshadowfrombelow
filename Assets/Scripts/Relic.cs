﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class Relic
{
    public string Name;
    public string Description;
    public bool Researched;

    public int TimesResolved;
    public Func<Relic, int, bool> ShouldRerollDie;
    public Func<Relic, int, int> ChangeValue;
    public int FoodReduction;
    public int DiceReduction;

    public Relic(string name, string desc, Func<Relic, int, bool> shouldReroll, int foodReduce = 0, int diceReduce = 0, Func<Relic, int, int> changeValue = null)
    {
        Name = name;
        Description = desc;
        ShouldRerollDie = shouldReroll;
        FoodReduction = foodReduce;
        DiceReduction = diceReduce;
        if (changeValue != null)
        {
            ChangeValue = changeValue;
        }
        else
        {
            ChangeValue = (x, y) => y;
        }
    }

    public static Relic Random()
    {
        var relics = new List<Relic>()
        {
        new Relic("Everturning Dial", "The first die with value at least 4 is rerolled each turn.",
                            (me, x) =>
                            {
                                if (me.TimesResolved == 0 && x > 3)
                                {
                                    me.TimesResolved++;
                                    return true;
                                }

                                return false;
                            }
                            ),
        new Relic("Elder Sign", "All fives are rerolled",
                            (me, x) =>
                            {
                                if (x == 5)
                                {
                                    me.TimesResolved++;
                                    return true;
                                }

                                return false;
                            }
                            ),
        new Relic("Ashen Bread", "Food gives 1 fewer die",
                            (me, x) => false,
                            1
                            ),
        new Relic("Noneuclidian Trapezohedron", "All dice values are reduced by one",
                            (me, x) =>
                            {
                                return false;
                            },
                            diceReduce: 1
                            ),
        new Relic("Arcane Scrawlings", "All 5s become 2s",
                            (me, x) => false,
                            changeValue: (me, x) =>
                            {
                                if (x == 5)
                                {
                                    return 2;
                                }

                                return x;
                            }
                            ),
        };

        return relics[UnityEngine.Random.Range(0, relics.Count)];
    }
}
