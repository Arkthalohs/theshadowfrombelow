﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class DiceRoller : MonoBehaviour
{
    public DieBehaviour DiePrefab;
    public DieDropBehaviour DropZonePrefab;
    public LayoutGroup DiceHolder;
    public DieDropBehaviour CrisisDieDrop;
    public Text CrisisName;
    public Text CrisisPenalty;
    public LayoutGroup RerollHolder;
    public Text RemainingRollsText;
    public Button RollButton;
    public Text AbilityDesc;
    public DieDropBehaviour AbilityDropZone;

    public Text RoomName;
    public Text RoomDescription;
    public LayoutGroup RoomDropZones;
    public RectTransform CharacterParent;
    public LayoutGroup OthersInRoom;

    public List<DieBehaviour> Results;
    public List<DieDropBehaviour> Inputs;
    public int DiceToRoll;

    private InteractableRoomBehaviour _room;
    private int _rollCount;
    private bool _initialRollComplete;
    private Character _character;
    private GameManagerBehaviour _manager;

    public void StartRoll()
    {
        if (_rollCount == _character.Rerolls)
        {
            return;
        }

        if (_rollCount == 0)
        {
            RollButton.interactable = false;
            StartCoroutine(RollDiceCoroutine(DiceToRoll));
        }
        else
        {
            RollButton.interactable = false;
            StartCoroutine(RerollDiceCoroutine());
        }

        _rollCount++;
        RemainingRollsText.text = (_character.Rerolls - _rollCount) + " Remaining";
    }

    public void FinishRoll()
    {
        gameObject.SetActive(false);
        var c = CharacterParent.GetComponentInChildren<CharacterBehaviour>().Character;
        c.Dice = Mathf.Max(1, c.Dice - 1);
        ////Destroy(_character);
        foreach (var d in Results)
        {
            if (d)
            {
                Destroy(d.gameObject);
            }
        }

        for (var i = 0; i < RoomDropZones.transform.childCount; i++)
        {
            Destroy(RoomDropZones.transform.GetChild(i).gameObject);
        }

        foreach (var i in Inputs)
        {
            Destroy(i.gameObject);
        }

        Inputs.Clear();
    }

    public void Show(GameManagerBehaviour game, CharacterBehaviour character, InteractableRoomBehaviour room)
    {
        _initialRollComplete = false;
        RollButton.interactable = true;
        DiceToRoll = character.Character.Dice;
        gameObject.SetActive(true);
        _room = room;
        _rollCount = 0;
        Results = new List<DieBehaviour>();

        RemainingRollsText.text = (character.Character.Rerolls - _rollCount) + " Remaining";
        CrisisDieDrop.transform.parent.gameObject.SetActive(room.HasCrisis);
        if (room.HasCrisis)
        {
            CrisisName.text = room.CurrentCrisis.Name;
            CrisisPenalty.text = room.CurrentCrisis.PenaltyText;
            CrisisDieDrop.OnDieDropped = die =>
            {
                Destroy(die.gameObject);
                room.CurrentCrisis.Difficulty -= die.Die.Value;
            };
        }

        RoomName.text = room.CurrentRoom.Name;
        RoomDescription.text = room.CurrentRoom.Description;
        for (var i = 0; i < room.CurrentRoom.DiceRequiredForAction; i++)
        {
            var dz = Instantiate(DropZonePrefab);
            Inputs.Add(dz);

            if (i < room.CurrentRoom.CurrentDiceValues.Count)
            {
                dz.OccupyingDie = room.CurrentRoom.CurrentDiceValues[i];
            }

            dz.transform.SetParent(RoomDropZones.transform);
            dz.transform.localScale = Vector3.one;
            dz.GetComponentInChildren<Text>().text = room.CurrentRoom.ShortDiceRequired[i % room.CurrentRoom.ShortDiceRequired.Length];
            var cur = i;
            dz.OnDieDropped = (die) =>
            {
                var input = Inputs.Select(x => x.OccupyingDie).ToList();
                input[cur] = die.Die.Value;
                var result = room.CurrentRoom.AreValidInputDice(input);
                if (result == Room.DiceResult.Failure)
                {
                    return;
                }

                Destroy(die.gameObject);
                dz.OccupyingDie = die.Die.Value;
                room.CurrentRoom.CurrentDiceValues = input;

                if (result == Room.DiceResult.Success)
                {
                    room.CurrentRoom.RoomAction(game, character, die.Die.Value);
                }
            };
        }

        AbilityDesc.text = character.Character.AbilityDescription;
        AbilityDropZone.gameObject.SetActive(character.Character.ClassAbility != null);
        if (character.Character.ClassAbility != null)
        {
            AbilityDropZone.GetComponentInChildren<Text>().text = character.Character.ShortAbilityDescription;
            AbilityDropZone.OccupyingDie = null;
            AbilityDropZone.OnDieDropped = (die) =>
            {
                var result = character.Character.CanUseAbility(die.Die.Value);
                if (!result)
                {
                    return;
                }

                Destroy(die.gameObject);
                AbilityDropZone.OccupyingDie = die.Die.Value;
                character.Character.ClassAbility(game, character);
            };
        }

        _character = character.Character;
        _manager = game;
        CharacterParent.GetComponentInChildren<CharacterBehaviour>().Character = character.Character;
        var inRoom = game.Survivors.Where(x => x.Character.CurrentRoom == room && character != x).ToList();
        for (var i = 0; i < OthersInRoom.transform.childCount; i++)
        {
            var c = OthersInRoom.transform.GetChild(i);
            c.gameObject.SetActive(i < inRoom.Count);
            if (i < inRoom.Count)
            {
                c.GetComponentInChildren<CharacterBehaviour>().Character = inRoom[i].Character;
            }
        }
    }

    public IEnumerator RollDiceCoroutine(int numDiceToRoll)
    {
        // TODO: Make this be real dice.
        yield return null;
        Results = new List<DieBehaviour>();
        ////DiceHolder.enabled = true;
        for (var i = 0; i < numDiceToRoll; i++)
        {
            var die = Instantiate(DiePrefab);
            die.Die = new Die(Random.Range(1, 7));
            die.transform.SetParent(RerollHolder.transform);
            die.transform.localScale = Vector3.one;
            die.DiceHold = DiceHolder;
            die.RerollDice = RerollHolder;
            Results.Add(die);
        }

        yield return RerollDiceCoroutine();
        RollButton.interactable = _rollCount < _character.Rerolls;
        _initialRollComplete = true;
    }

    public IEnumerator RerollDiceCoroutine()
    {
        yield return null;
        var toRoll = Results.Where(x => x && !x.Die.Locked).ToList();

        foreach (var die in toRoll)
        {
            die.Rolling = true;
        }

        yield return RollDice(toRoll);

        foreach (var die in toRoll)
        {
            die.transform.rotation = Quaternion.identity;
        }

        yield return HandleRelics(toRoll);

        foreach (var die in toRoll)
        {
            die.Rolling = false;
            die.transform.rotation = Quaternion.identity;
        }

        RollButton.interactable = _rollCount < _character.Rerolls;
    }

    private IEnumerator HandleRelics(List<DieBehaviour> dice)
    {
        var rerolling = new List<DieBehaviour>();
        foreach (var d in dice)
        {
            foreach (var r in _character.Relics)
            {
                var newValue = r.ChangeValue(r, d.Die.Value);
                if (newValue != d.Die.Value)
                {
                    d.Die.Value = newValue;
                    d.GetComponentInChildren<Text>().text = d.Die.Value.ToString();

                    d.GetComponentInChildren<Image>().CrossFadeColor(new Color(.8f, 0, .7f), .25f, false, true);
                }

                if (r.DiceReduction > 0)
                {
                    d.Die.Value = Mathf.Max(1, d.Die.Value - r.DiceReduction);
                    d.GetComponentInChildren<Text>().text = d.Die.Value.ToString();

                    d.GetComponentInChildren<Image>().CrossFadeColor(new Color(.8f, 0, .7f), .25f, false, true);
                }
            }

            foreach (var r in _character.Relics)
            {
                if (r.ShouldRerollDie(r, d.Die.Value))
                {
                    rerolling.Add(d);
                    d.GetComponentInChildren<Image>().CrossFadeColor(new Color(.8f, 0, .7f), .25f, false, true);
                }
            }
        }

        if (rerolling.Count > 0)
        {
            yield return new WaitForSeconds(.2f);
            yield return RollDice(rerolling);
        }
    }

    private IEnumerator RollDice(List<DieBehaviour> dice)
    {
        ////foreach (var die in dice)
        ////{
        ////    if (die.Die.Locked)
        ////    {
        ////        continue;
        ////    }

        ////    die.Die.Value = Random.Range(1, 6);
        ////    die.GetComponentInChildren<Text>().text = die.Die.Value.ToString();
        ////    die.transform.rotation = Quaternion.Euler(0, 0, -22 + Random.Range(0, 44));
        ////    yield return null;
        ////}
        var startTime = Time.time;
        while (Time.time < startTime + .25f)
        {
            foreach (var die in dice)
            {
                die.Die.Value = Random.Range(1, 7);
                die.GetComponentInChildren<Text>().text = die.Die.Value.ToString();
                die.transform.rotation = Quaternion.Euler(0, 0, -22 + Random.Range(0, 45));
            }

            yield return null;
        }
    }

    private void Update()
    {
        if (_room == null)
        {
            return;
        }

        CrisisDieDrop.GetComponentInChildren<Text>().text = _room.HasCrisis ? _room.CurrentCrisis.Difficulty.ToString() : "";
        CrisisDieDrop.transform.parent.gameObject.SetActive(_room.HasCrisis);

        Results = Results.Where(x => x).ToList();
        ////if (Results.Count == 0  && _initialRollComplete)
        ////{
        ////    FinishRoll();
        ////}

        if (!_room.CurrentRoom.CanShow(_manager, CharacterParent.GetComponentInChildren<CharacterBehaviour>()))
        {
            RoomDescription.text = _room.CurrentRoom.CantShowText(_manager, CharacterParent.GetComponentInChildren<CharacterBehaviour>());
            foreach (var dropZone in Inputs)
            {
                dropZone.OccupyingDie = -1;
            }
        }
    }

    ////public IEnumerator RerollDiceCoroutine()
    ////{
    ////    yield return null;
    ////    foreach (var die in Results)
    ////    {
    ////        if (!die.Locked)
    ////        {
    ////            die.Value = Random.Range(1, 6);
    ////        }
    ////    }
    ////}

    public class Die
    {
        public int Value;
        public bool Locked;

        public Die(int val)
        {
            Value = val;
        }
    }
}
