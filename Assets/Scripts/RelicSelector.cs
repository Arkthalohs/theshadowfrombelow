﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class RelicSelector : MonoBehaviour
{
    public LayoutGroup ScrollContent;
    public Button ButtonPrefab;

    public void Show(CharacterBehaviour character, Action<Relic> onclick)
    {
        Show(character, character.Character.Relics, onclick);
    }

    public void Show(CharacterBehaviour character, List<Relic> relics, Action<Relic> onclick)
    {
        gameObject.SetActive(true);
        for (var i = 0; i < ScrollContent.transform.childCount; i++)
        {
            Destroy(ScrollContent.transform.GetChild(i).gameObject);
        }

        foreach (var r in relics)
        {
            var butt = Instantiate(ButtonPrefab);
            butt.transform.Find("Name").GetComponent<Text>().text = r.Name;
            butt.transform.Find("Desc").GetComponent<Text>().text = r.Researched ? r.Description : "Unknown Effect";
            butt.transform.Find("Researched").gameObject.SetActive(r.Researched);
            butt.transform.SetParent(ScrollContent.transform);
            butt.transform.localScale = Vector3.one;
            var relic = r;
            butt.onClick.AddListener(new UnityEngine.Events.UnityAction(() =>
            {
                onclick(relic);
                gameObject.SetActive(false);
            }));
        }
    }
}
