﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class MouseManagerBehaviour : MonoBehaviour
{
    public Color DefaultColor;
    public Color DangerColor;
    public Color HighlightColor;
    public Color DangerHighlightColor;
    public GameManagerBehaviour GameManager;

    private InteractableRoomBehaviour _currentHoverTarget;

    // Update is called once per frame
    private void Update()
    {
        RaycastHit raycastHit;
        if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out raycastHit))
        {
            var newRoom = raycastHit.transform.GetComponentInParent<InteractableRoomBehaviour>();
            HoverRoom(newRoom);
            
            if (Input.GetMouseButtonDown(0) && _currentHoverTarget)
            {
                GameManager.MoveActiveCharacterToRoom(_currentHoverTarget);
                foreach (var room in GameManager.Rooms)
                {
                    room.SetColors(DefaultColor);
                }
            }
        }
        else if (_currentHoverTarget != null)
        {
            _currentHoverTarget.SetColors(ColorToSet(_currentHoverTarget));
        }
    }

    private void HoverRoom(InteractableRoomBehaviour newRoom)
    {
        if (_currentHoverTarget != newRoom)
        {
            if (_currentHoverTarget != null)
            {
                _currentHoverTarget.SetColors(ColorToSet(_currentHoverTarget));
            }

            _currentHoverTarget = newRoom;
        }

        // TODO: Hover glow
        if (GameManager.ActiveCharacter != null && _currentHoverTarget)
        {
            _currentHoverTarget.SetColors(GameManager.ActiveCharacter.Character.CurrentRoom.GetPathToTargetRoom(_currentHoverTarget).Any(x => x.HasCrisis)
                ? DangerHighlightColor
                : HighlightColor);
        }
    }

    private Color ColorToSet(InteractableRoomBehaviour room)
    {
        if (GameManager.ActiveCharacter == null)
        {
            return DefaultColor;
        }

        return GameManager.ActiveCharacter.Character.CurrentRoom.GetPathToTargetRoom(room).Any(x => x.HasCrisis)
                ? DangerColor
                : DefaultColor;
    }
}
