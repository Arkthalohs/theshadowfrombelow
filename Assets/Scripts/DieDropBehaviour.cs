﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Linq;

public class DieDropBehaviour : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public Image Glow;
    public Image OccupyingDieImage;
    public int? OccupyingDie;
    public Action<DieBehaviour> OnDieDropped;

    private float _lastTime;
    private float _minAlpha = .4f;
    private float _maxAlpha = .7f;
    private bool _toMax;

    public void OnPointerEnter(PointerEventData eventData)
    {
        _minAlpha = .7f;
        _maxAlpha = 1f;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        _minAlpha = .4f;
        _maxAlpha = .7f;
    }

    // Update is called once per frame
    public void Update()
    {
        if (Time.time > _lastTime + .5f)
        {
            _lastTime = Time.time;
            _toMax = !_toMax;
            Glow.CrossFadeColor(new Color(Glow.color.r, Glow.color.g, Glow.color.b, _toMax ? _maxAlpha : _minAlpha), .5f, false, true);
        }

        Glow.gameObject.SetActive(!OccupyingDie.HasValue);
        if (OccupyingDieImage)
        {
            OccupyingDieImage.gameObject.SetActive(OccupyingDie.HasValue && OccupyingDie.Value > 0);
            if (OccupyingDie.HasValue && OccupyingDie.Value > 0)
            {
                OccupyingDieImage.GetComponentInChildren<Text>().text = OccupyingDie.ToString();
            }
        }
    }
}
