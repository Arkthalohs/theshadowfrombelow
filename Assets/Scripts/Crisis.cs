﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class Crisis
{
    public int Difficulty;
    public string Name;
    public string PenaltyText;
    public int Progress;
    public Action<GameManagerBehaviour> TurnComplete;

    private Crisis(string name, string penaltyText, int diff, Action<GameManagerBehaviour> turnComplete)
    {
        Name = name;
        Difficulty = diff;
        PenaltyText = penaltyText;
        TurnComplete = turnComplete;
    }

    public static Crisis Random(int challengeLevel, int difficulty)
    {
        var types = new List<Penalty>();
        types.Add(new Penalty("-" + (10 * difficulty) + "°C",
                manager =>
                {
                    manager.Temperature -= 10 * difficulty;
                }));
        types.Add(new Penalty("-1 HP All",
                manager =>
                {
                    foreach (var s in manager.Survivors)
                    {
                        s.Character.Health--;
                    }
                }));
        types.Add(new Penalty("-1 Die All",
                manager =>
                {
                    foreach (var s in manager.Survivors)
                    {
                        s.Character.Dice = Mathf.Max(1, s.Character.Dice - 1);
                    }
                }));
        types.Add(new Penalty("-1 Food",
                manager =>
                {
                    manager.Food = Math.Max(0, manager.Food - 1);
                }));
        types.Add(new Penalty("-1 Fuel",
                manager =>
                {
                    manager.FuelGathered = Math.Max(0, manager.FuelGathered - 1);
                }));

        var names = new List<string>()
        {
            "Wolf Attack",
            "Shoggoth",
            "Broken Windows",
            "Blizzard",
            "Heavy Winds",
            "Shambler",
            "Living Shadows",
            "Unknown Colour",
            "Cultist"
        };

        var penalty = types[UnityEngine.Random.Range(0, types.Count)];
        return new Crisis(
            names[UnityEngine.Random.Range(0, names.Count)],
            penalty.Description,
            challengeLevel,
            penalty.TurnComplete
            );
    }

    public static Crisis BlownInWindows
    {
        get
        {
            return new Crisis(
                "Broken Windows",
                "-10°C",
                UnityEngine.Random.Range(5, 10),
                manager =>
                {
                    manager.Temperature -= 10;
                });
        }
    }

    public static Crisis WolfAttack
    {
        get
        {
            return new Crisis(
                "Wolf Attack",
                "-1 HP All",
                UnityEngine.Random.Range(10, 20),
                manager =>
                {
                    foreach (var s in manager.Survivors)
                    {
                        s.Character.Health--;
                    }
                });
        }
    }

    private class Penalty
    {
        public string Description;
        public Action<GameManagerBehaviour> TurnComplete;

        public Penalty(string d, Action<GameManagerBehaviour> penalty)
        {
            Description = d;
            TurnComplete = penalty;
        }
    }
}
