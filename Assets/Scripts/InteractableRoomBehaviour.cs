﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class InteractableRoomBehaviour : MonoBehaviour
{
    public List<InteractableRoomBehaviour> AdjacentRooms;
    public RectTransform CharacterContainer;
    public Transform CrisisView;
    public Crisis CurrentCrisis;
    public Room.RoomType RoomType;
    public Room CurrentRoom;

    public bool HasCrisis
    {
        get
        {
            return CurrentCrisis != null && !string.IsNullOrEmpty(CurrentCrisis.Name);
        }
    }

    public void SetColors(Color hoverColor)
    {
        foreach (var renderer in GetComponentsInChildren<MeshRenderer>())
        {
            renderer.material.color = hoverColor;
        }
    }

    public List<InteractableRoomBehaviour> GetPathToTargetRoom(InteractableRoomBehaviour targetRoom)
    {
        // TODO: Damage from moving through damaged rooms
        ////if (this == targetRoom)
        ////{
        ////    return new List<InteractableRoomBehaviour>();
        ////}

        var openSet = new List<InteractableRoomBehaviour>();
        openSet.Add(this);
        var cameFrom = new Dictionary<InteractableRoomBehaviour, InteractableRoomBehaviour>();
        var gScores = new Dictionary<InteractableRoomBehaviour, int>();
        gScores[this] = 0;
        while (openSet.Count > 0)
        {
            var current = openSet[0];

            openSet.Remove(current);
            foreach (var neighbor in current.AdjacentRooms)
            {
                var gScore = gScores[current] + (neighbor.HasCrisis ? 100000 : 1);
                if (!openSet.Contains(neighbor) && (!gScores.ContainsKey(neighbor) || gScore < gScores[neighbor]))
                {
                    openSet.Add(neighbor);
                }

                if (gScores.ContainsKey(neighbor) && gScore >= gScores[neighbor])
                {
                    continue;
                }

                cameFrom[neighbor] = current;
                gScores[neighbor] = gScore;
            }
        }

        var totalPath = new List<InteractableRoomBehaviour>();
        ////totalPath.Add(targetRoom);
        var cur = targetRoom;
        while (cameFrom.ContainsKey(cur))
        {
            cur = cameFrom[cur];
            totalPath.Add(cur);
        }

        return totalPath;
    }

    private void Start()
    {
        CurrentRoom = Room.GetRoomForType(RoomType);
        CrisisView = CharacterContainer.parent.Find("Crisis");
        CrisisView.gameObject.SetActive(false);
    }

    private void Update()
    {
        GetComponent<LineRenderer>().enabled = CharacterContainer.childCount > 0;
        if (HasCrisis)
        {
            if (CurrentCrisis.Difficulty <= 0)
            {
                CurrentCrisis = null;
                return;
            }

            CrisisView.gameObject.SetActive(HasCrisis);
            var texts = CrisisView.GetComponentsInChildren<Text>();
            texts[0].text = CurrentCrisis.Difficulty.ToString();
            texts[1].text = CurrentCrisis.PenaltyText;
            texts[2].text = CurrentCrisis.Name;
        }
        else
        {
            CrisisView.gameObject.SetActive(HasCrisis);
        }
    }
}
