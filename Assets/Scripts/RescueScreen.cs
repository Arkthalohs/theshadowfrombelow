﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RescueScreen : MonoBehaviour
{
    public Text FuelCost;
    public Button Confirm;
    public LayoutGroup Rescue;
    public LayoutGroup Leave;
    public GameManagerBehaviour Game;

    public void Show(GameManagerBehaviour game)
    {
        gameObject.SetActive(true);
        Game = game;

        var characters = Leave.GetComponentsInChildren<CharacterBehaviour>();

        for (var i = 0; i < characters.Length; i++)
        {
            characters[i].gameObject.SetActive(i < game.Survivors.Count);
            if (i < game.Survivors.Count)
            {
                characters[i].Character = game.Survivors[i].Character;
            }
        }
    }

    public void Complete()
    {
        gameObject.SetActive(false);
        for (var i = 0; i < Leave.transform.childCount; i++)
        {
            Destroy(Game.Survivors.Find(x => x.Character == Leave.transform.GetChild(i).GetComponent<CharacterBehaviour>().Character));
        }

        Game.EndScreen.EndGame(Game, new List<CharacterBehaviour>(Rescue.transform.GetComponentsInChildren<CharacterBehaviour>()));
    }

    private void Update()
    {
        FuelCost.text = "Fuel Cost: " + Rescue.transform.childCount + "(" + Game.FuelGathered + " available)";
        FuelCost.color = Rescue.transform.childCount <= Game.FuelGathered ? Color.black : Color.red;
        Confirm.interactable = Rescue.transform.childCount <= Game.FuelGathered;
    }
}
