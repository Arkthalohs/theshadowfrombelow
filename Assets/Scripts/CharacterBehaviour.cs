﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class CharacterBehaviour : MonoBehaviour
{
    public Character Character;

    public Image Glow;
    public Text Name;
    public Text Class;
    public RectTransform DiceContainer;
    public Image Portrait;
    public RectTransform TurnTakenPanel;
    public RectTransform HealthContainer;
    public RectTransform RelicScreen;
    public Text RelicsText;

    private void Update()
    {
        for (var i = 0; i < DiceContainer.childCount; i++)
        {
            DiceContainer.GetChild(i).GetChild(0).GetComponent<Image>().enabled = Character.Dice >= (Character.MaxDice - i);
        }

        for (var i = 0; i < HealthContainer.childCount; i++)
        {
            HealthContainer.GetChild(i).GetChild(0).GetComponent<Image>().enabled = Character.Health >= (Character.MaxHealth - i);
        }

        Name.text = Character.Name;
        Class.text = Character.Class;
        TurnTakenPanel.gameObject.SetActive(Character.TurnTaken);
        RelicsText.gameObject.SetActive(Character.Relics.Count > 0);
        RelicsText.color = Character.Relics.Count(x => !x.Researched) == 0 ? new Color(.1f, .8f, .1f) : new Color(.9f, .1f, .9f);
        RelicsText.text = Character.Relics.Count + " Relic" + (Character.Relics.Count == 1 ? "" : "s");

        if (Character.Health == 0)
        {
            Destroy(gameObject);
        }
    }

    public void GiveRelic(Relic relic)
    {
        Character.Relics.Add(relic);
        RelicScreen.gameObject.SetActive(true);
        var texts = RelicScreen.GetComponentsInChildren<Text>();
        texts[0].text = Character.Name + " has found a Relic";
        texts[1].text = relic.Name;
        texts[2].text = relic.Researched ? relic.Description : "You do not yet know what havoc it shall wreck.";
    }
}
