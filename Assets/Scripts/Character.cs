﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class Character
{
    public const int MaxDice = 5;
    public const int MaxHealth = 6;

    public InteractableRoomBehaviour CurrentRoom;
    public int Health;
    public int Dice;
    public string Name;
    public string Class;
    public bool TurnTaken;
    public List<Relic> Relics;
    public string AbilityDescription;
    public string ShortAbilityDescription;
    public Predicate<int> CanUseAbility;
    public Action<GameManagerBehaviour, CharacterBehaviour> ClassAbility;
    public int Rerolls;
    public bool ColdResistant;

    public Character(string name, string className,
        string classAbilityDesc, string dieReq, Predicate<int> canUse, Action<GameManagerBehaviour, CharacterBehaviour> ability,
        int rerolls = 2, bool coldResist = false)
    {
        Name = name;
        Class = className;
        Dice = MaxDice;
        Health = MaxHealth;
        Rerolls = rerolls;
        AbilityDescription = classAbilityDesc;
        ShortAbilityDescription = dieReq;
        ClassAbility = ability;
        CanUseAbility = canUse;
        Relics = new List<Relic>();
        ColdResistant = coldResist;
    }
}
