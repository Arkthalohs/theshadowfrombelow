﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class DieBehaviour : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerDownHandler, IPointerUpHandler
{
    public bool FollowMouse;
    public LayoutGroup DiceHold;
    public LayoutGroup RerollDice;
    public DiceRoller.Die Die;
    public bool Rolling;
    private bool _mouseInside;
    private Vector2 _offset;
    private Vector3 _startPosition;

    public void OnPointerDown(PointerEventData eventData)
    {
        FollowMouse = !Rolling && true;
        _offset = transform.position - Input.mousePosition;
        _startPosition = transform.position;
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        _mouseInside = true;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        _mouseInside = false;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        // This is terrible.
        var dropZone = FindObjectsOfType<DieDropBehaviour>().FirstOrDefault(x => Intersect(x.GetComponent<RectTransform>()));
        if (dropZone && !dropZone.OccupyingDie.HasValue)
        {
            dropZone.OnDieDropped(this);
        }

        if (Intersect(DiceHold.GetComponent<RectTransform>()))
        {
            Die.Locked = true;
            if (transform.parent == DiceHold.transform)
            {
                transform.position = _startPosition;
            }
            else
            {
                transform.SetParent(DiceHold.transform);
            }
        }
        else if (Intersect(RerollDice.GetComponent<RectTransform>()))
        {
            Die.Locked = false;
            if (transform.parent == RerollDice.transform)
            {
                transform.position = _startPosition;
            }
            else
            {
                transform.SetParent(RerollDice.transform);
            }
        }
        else
        {
            transform.position = _startPosition;
        }

        FollowMouse = false;
        ////GetComponentInChildren<Image>().color = Die.Locked ? Color.cyan : Color.white;
    }

    private bool Intersect(RectTransform rect)
    {
        var rt = GetComponent<RectTransform>();
        var corners = new Vector3[4];
        rt.GetWorldCorners(corners);
        var rect1 = new Rect(corners[0].x, corners[0].y, corners[2].x - corners[0].x, corners[2].y - corners[0].y);
        rect.GetWorldCorners(corners);
        var rect2 = new Rect(corners[0].x, corners[0].y, corners[2].x - corners[0].x, corners[2].y - corners[0].y);
        return rect1.Overlaps(rect2);
    }

    private void Update()
    {
        if (FollowMouse)
        {
            transform.position = Input.mousePosition + (Vector3)_offset;
        }
    }
}
