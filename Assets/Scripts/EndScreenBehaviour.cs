﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class EndScreenBehaviour : MonoBehaviour
{
    public LayoutGroup ScrollContent;
    public Text FinalResult;
    public Text TextPrefab;

    public void EndGame(GameManagerBehaviour game, List<CharacterBehaviour> survivors)
    {
        gameObject.SetActive(true);
        StartCoroutine(EndGameCoroutine(game, survivors));
    }

    public void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void Quit()
    {
        Application.Quit();
    }

    public IEnumerator EndGameCoroutine(GameManagerBehaviour game, List<CharacterBehaviour> survivors)
    {
        var result = 0;
        for (var i = 100; i < game.ExpeditionCost; i += 100)
        {
            var text = Instantiate(TextPrefab);
            text.transform.SetParent(ScrollContent.transform);
            text.transform.localScale = Vector3.one;
            text.text = "Research base repair cost: -$100";
            result -= 100;
            FinalResult.text = (result < 0 ? "-$" : "$") + Mathf.Abs(result);
            FinalResult.color = result < 0 ? Color.red : Color.green;
            yield return new WaitForSeconds(.2f);
        }

        for (var i = survivors.Count; i < 4; i++)
        {
            var text = Instantiate(TextPrefab);
            text.transform.SetParent(ScrollContent.transform);
            text.transform.localScale = Vector3.one;
            text.text = "Loss of Life: -$500";
            result -= 500;
            FinalResult.text = (result < 0 ? "-$" : "$") + Mathf.Abs(result);
            FinalResult.color = result < 0 ? Color.red : Color.green;
            yield return new WaitForSeconds(.2f);
        }

        foreach (var relic in survivors.SelectMany(s => s.Character.Relics))
        {
            var text = Instantiate(TextPrefab);
            text.transform.SetParent(ScrollContent.transform);
            text.transform.localScale = Vector3.one;
            text.text = relic.Name + ": $100";
            result += 100;
            FinalResult.text = (result < 0 ? "-$" : "$") + Mathf.Abs(result);
            FinalResult.color = result < 0 ? Color.red : Color.green;
            if (relic.Researched)
            {
                yield return new WaitForSeconds(.2f);
                var t2 = Instantiate(TextPrefab);
                t2.transform.SetParent(ScrollContent.transform);
                t2.transform.localScale = Vector3.one;
                t2.text = "  Knowing what the " + relic.Name + " does: $200";
                result += 100;
                FinalResult.text = (result < 0 ? "-$" : "$") + Mathf.Abs(result);
                FinalResult.color = result < 0 ? Color.red : Color.green;
            }

            yield return new WaitForSeconds(.2f);
        }

    }
}
