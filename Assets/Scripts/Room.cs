﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

[Serializable]
public class Room
{
    public string Name;
    public string Description;
    public Action<GameManagerBehaviour, CharacterBehaviour, int> RoomAction;
    public string[] ShortDiceRequired;
    public int DiceRequiredForAction;
    public Func<List<int?>, DiceResult> AreValidInputDice;
    public List<int?> CurrentDiceValues;
    public Func<GameManagerBehaviour, CharacterBehaviour, bool> CanShow;
    public Func<GameManagerBehaviour, CharacterBehaviour, string> CantShowText;

    public Room(string name, string desc, string[] shortDice, int diceReq, 
        Action<GameManagerBehaviour, CharacterBehaviour, int> action,
        Func<List<int?>, DiceResult> diceFunc,
        Func<GameManagerBehaviour, CharacterBehaviour, bool> canShow = null, 
        Func<GameManagerBehaviour, CharacterBehaviour, string> cantShowText = null)
    {
        Name = name;
        Description = desc;
        ShortDiceRequired = shortDice;
        RoomAction = action;
        DiceRequiredForAction = diceReq;
        AreValidInputDice = diceFunc;
        CurrentDiceValues = new List<int?>();
        if (canShow == null)
        {
            CanShow = (x, y) => true;
        }
        else
        {
            CanShow = canShow;
        }

        CantShowText = cantShowText;
    }
    
    public enum RoomType
    {
        Dome,
        Garage,
        Heat,
        Cargo,
        PodA,
        PodB,
        Digsite,
        PowerPlant
    }

    public enum DiceResult
    {
        Success,
        PartialSuccess,
        Failure
    }

    public static Room GetRoomForType(RoomType type)
    {
        switch (type)
        {
            case RoomType.Dome:
                return new Room(
                    "Dome",
                    "4 or greater: Research a relic",
                    new string[] { "4↑" },
                    1,
                    (manager, character, lastValue) =>
                    {
                        manager.RelicSelector.Show(character, character.Character.Relics.Where(x => !x.Researched).ToList(), r =>
                        {
                            r.Researched = true;
                            manager.RelicScreen.gameObject.SetActive(true);
                            var texts = manager.RelicScreen.GetComponentsInChildren<Text>();
                            texts[0].text = "You have deciphered the effect of a relic!";
                            texts[1].text = r.Name;
                            texts[2].text = r.Researched ? r.Description : "You do not yet know what havoc it shall wreck.";
                        });
                    },
                    (dice) =>
                    {
                        return dice.TrueForAll(x => x == null || x >= 4) ? DiceResult.Success : DiceResult.Failure;
                    },
                    (game, character) =>
                    {
                        return character.Character.Relics.Where(x => !x.Researched).ToList().Count > 0;
                    },
                    (game, character) =>
                    {
                        if (character.Character.Relics.Count <= 0)
                        {
                            return "This character doesn't have any relics to research.";
                        }

                        if (character.Character.Relics.TrueForAll(r => r.Researched))
                        {
                            return "All relics are already researched";
                        }

                        return "";
                    }
                    );
            case RoomType.Cargo:
                return new Room(
                    "Cargo Hold",
                    "3 or 4: Gain 1 fuel\n5 or 6: Gain 1 food",
                    new string[] { "3/4", "3/4", "5/6", "5/6" },
                    4,
                    (manager, character, lastValue) =>
                    {
                        if (lastValue == 3 || lastValue == 4)
                        {
                            manager.FuelGathered++;
                        }
                        else
                        {
                            manager.Food++;
                        }
                    },
                    (dice) =>
                    {
                        ////Debug.Log(dice.Count + " " + dice[0] + " " + dice[1] + " " + dice[2]);
                        ////return dice.TrueForAll(x => x == null || x >= 5) ? DiceResult.Success : DiceResult.Failure;
                        if ((dice[0] == null || dice[0] == 3 || dice[0] == 4) &&
                        (dice[1] == null || dice[1] == 3 || dice[1] == 4) &&
                        (dice[2] == null || dice[2] == 5 || dice[2] == 6) &&
                        (dice[3] == null || dice[3] == 5 || dice[3] == 6))
                        {
                            return DiceResult.Success;
                        }

                        return DiceResult.Failure;
                    }
                    );
            case RoomType.PodA:
                return new Room(
                    "Pod A",
                    "5 or greater: +2 Dice to Everyone in the Room",
                    new string[] { "5↑" },
                    2,
                    (manager, character, lastValue) =>
                    {
                        var inThisRoom = manager.Survivors.Where(x => x.Character.CurrentRoom.CurrentRoom.Name == "Pod A");
                        foreach (var s in inThisRoom)
                        {
                            s.Character.Dice = Math.Min(s.Character.Dice + 2, Character.MaxDice);
                        }
                    },
                    (dice) =>
                    {
                        return dice.TrueForAll(x => x == null || x >= 5) ? DiceResult.Success : DiceResult.Failure;
                    }
                    );
            case RoomType.PodB:
                return new Room(
                    "Pod B",
                    "5 or greater: +2 Health to Everyone in the Room",
                    new string[] { "5↑" },
                    2,
                    (manager, character, lastValue) =>
                    {
                        var inThisRoom = manager.Survivors.Where(x => x.Character.CurrentRoom.CurrentRoom.Name == "Pod B");
                        foreach (var s in inThisRoom)
                        {
                            s.Character.Health = Math.Min(s.Character.Health + 2, Character.MaxHealth);
                        }
                    },
                    (dice) =>
                    {
                        return dice.TrueForAll(x => x == null || x >= 5) ? DiceResult.Success : DiceResult.Failure;
                    }
                    );
            case RoomType.Heat:
                return new Room(
                    "Furnace",
                    "1, 2, 3: Increase temp by 10°C",
                    new string[] { "1", "2", "3" },
                    3,
                    (manager, character, lastValue) =>
                    {
                        manager.Temperature = Mathf.Min(40, manager.Temperature + 10);
                    },
                    (dice) =>
                    {
                        if ((dice[0] == null || dice[0] == 1)
                        && (dice[1] == null || dice[1] == 2)
                        && (dice[2] == null || dice[2] == 3))
                        {
                            return DiceResult.Success;
                        }

                        return DiceResult.Failure;
                    },
                    (manager, character) =>
                    {
                        return manager.Temperature < 40;
                    },
                    (x, y) => "Turn it up any hotter and we'll all roast!"
                    );
            case RoomType.Garage:
                return new Room(
                    "Garage",
                    "1 or greater: Destroy a relic, lose 1 fuel",
                    new string[] { "1↑" },
                    3,
                    (manager, character, lastValue) =>
                    {
                        manager.FuelGathered--;
                        manager.RelicSelector.Show(character, r =>
                        {
                            character.Character.Relics.Remove(r);
                        });
                    },
                    (dice) =>
                    {
                        return DiceResult.Success;
                    },
                    (game, character) =>
                    {
                        return game.FuelGathered > 0 && character.Character.Relics.Count > 0;
                    },
                    (game, character) =>
                    {
                        if (game.FuelGathered <= 0)
                        {
                            return "Insufficient fuel to drive a relic out into the snow";
                        }
                        else if (character.Character.Relics.Count <= 0)
                        {
                            return "This character doesn't have any relics to dump.";
                        }

                        return "";
                    }
                    );
            case RoomType.PowerPlant:
                return new Room(
                    "Room",
                    "",
                    new string[] { "" },
                    1,
                    (manager, character, lastValue) =>
                    {

                    },
                    (dice) =>
                    {
                        return DiceResult.Failure;
                    }
                    );
            case RoomType.Digsite:
                return new Room(
                    "Digsite",
                    "Match dice numbers to gain a relic on this character.",
                    new string[] { "=" },
                    2,
                    (manager, character, lastValue) =>
                    {
                        Debug.Log("Relic!");
                        character.GiveRelic(Relic.Random());
                    },
                    (dice) =>
                    {
                        if (!dice.TrueForAll(x => x == null || x == dice.First(y => y.HasValue).Value))
                        {
                            return DiceResult.Failure;
                        }

                        if (dice.Count(x => x != null) < 2)
                        {
                            return DiceResult.PartialSuccess;
                        }

                        return DiceResult.Success;
                    }
                    );
            default:
                return new Room(
                    "Room",
                    "",
                    new string[] { "" },
                    1,
                    (manager, character, lastValue) =>
                    {

                    },
                    (dice) =>
                    {
                        return DiceResult.Failure;
                    }
                    );
        }
    }
}
