﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class FixParentPositions
{
    [MenuItem("Tools/Fix Hierarchy Positions")]
    public static void FixPositions()
    {
        var parent = Selection.activeTransform;
        if (parent.childCount == 0)
        {
            return;
        }

        var center = Vector3.zero;
        var positions = new Dictionary<Transform, Vector3>();
        for (var i = 0; i < parent.childCount; i++)
        {
            var child = parent.GetChild(i);
            positions[child] = child.position;
            center += child.position / parent.childCount;
        }

        parent.position = center;
        foreach (var keyPair in positions)
        {
            keyPair.Key.position = keyPair.Value;
        }
    }

    [MenuItem("Tools/Center Label &c")]
    public static void CenterLabelOnTransform()
    {
        if (Selection.transforms.Length != 2)
        {
            return;
        }

        var label = Selection.transforms.First(x => x.GetComponentInChildren<Text>());
        var other = Selection.transforms.First(x => x != label.transform);
        label.transform.position = RectTransformUtility.WorldToScreenPoint(Camera.main, other.position);
        label.transform.GetComponentInChildren<Text>().transform.rotation = other.transform.rotation;
    }

    [MenuItem("Tools/Reparent Label &p")]
    public static void MoveLabelUnderNewParent()
    {
        foreach (var trans in Selection.transforms)
        {
            var newParent = new GameObject();
            Undo.RegisterCreatedObjectUndo(newParent, "Created go");
            Undo.AddComponent<RectTransform>(newParent);
            Undo.SetTransformParent(newParent.transform, trans.parent, "Reparent");
            Undo.RecordObject(trans.gameObject, "Rename");
            newParent.name = trans.name;
            newParent.transform.position = trans.position;
            Undo.SetTransformParent(trans, newParent.transform, "Reparent");
            trans.name = "Label";
        }
    }

    [MenuItem("Tools/Setup LineRenderer &l")]
    public static void SetupLineRenderer()
    {
        foreach (var go in Selection.gameObjects)
        {
            var room = go.GetComponentInParent<InteractableRoomBehaviour>();
            var lr = room.GetComponent<LineRenderer>();
            if (!lr)
            {
                lr = Undo.AddComponent<LineRenderer>(room.gameObject);
            }

            var targetEnd = Camera.main.ScreenToWorldPoint(room.CharacterContainer.transform.position);
            targetEnd -= Vector3.forward * targetEnd.z;

            lr.useWorldSpace = true;
            lr.startWidth = lr.endWidth = 0.05f;
            lr.numCapVertices = 5;
            lr.SetPositions(new Vector3[]
            {
                go.transform.position - Vector3.forward,
                targetEnd - Vector3.forward
            });
        }
    }
}
